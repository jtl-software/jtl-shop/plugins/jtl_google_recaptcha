<h2>{__('Testing')}</h2>
{if $reCaptchaConfig}
    <p>{__('This will testing the captcha')}</p>
    {if isset($reCaptchaValid)}
        {if $reCaptchaValid}
            <div class="alert alert-success">
                {__('Captcha is valid')}<br>
                {__('Some number')}:&nbsp;{$reCaptchaTest}
            </div>
        {else}
            <div class="alert alert-warning">
                {__('Captcha is invalid')}
            </div>
        {/if}
    {/if}
    <form method="post" action="{$pluginURL}" name="captcha_testing">
        {$jtl_token}
        <input type="hidden" name="kPlugin" value="{$kPlugin}"/>
        <input type="hidden" name="kPluginAdminMenu" value="{$kPluginAdminMenu}"/>
        <div class="form-group form-row align-items-center">
            <label class="col-form-label" for="test_var">{__('Some number')}</label>
            <div class="col col-sm-4">
                <input class="form-control"
                       id="test_var"
                       name="reCaptchaTest[test_var]"
                       type="text"
                       maxlength="32"
                       required aria-required="true"/>
            </div>
            <input class="btn btn-default" type="submit" value="{__('Send')}">
        </div>
        {$reCaptcha}
    </form>
{else}
    {__('Not configured')}
{/if}
