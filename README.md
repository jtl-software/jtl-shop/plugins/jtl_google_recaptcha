# JTL Google reCaptcha v2

Dieses Plugin integriert Google reCaptcha v2 als Spamschutz in Ihren JTL-Shop.
Sie müssen Ihre Domain auf https://www.google.com/recaptcha registrieren. Anschließend erhalten Sie von Google Ihren
Website- und Geheimen Schlüssel.

Das Plugin arbeitet unter dem Aspekt der Datenvermeidung.
Das Laden der reCaptcha-API von *https://www.google.com/recaptcha/* erfolgt erst,
wenn der Anwender das Formular absendet. Das Google reCaptcha wird dann in einem modalen PopUp dargestellt.

Solange der Anwender nicht aktiv ein Formular absendet, werden keine Cookies gesetzt oder anderweitig Daten an
Dritt-Server übertragen.

# Changelog

## 1.2.0

* Kompatibilität zu JTL-Shop 5.5.0 und PHP 8.4 hergestellt

## 1.1.0

* Kompatibilität zu JTL-Shop 5.2.0 hergestellt
* Hierarchie von Überschriften überarbeitet
