<?php

declare(strict_types=1);

namespace Plugin\jtl_google_recaptcha;

use JTL\Plugin\PluginInterface;

/**
 * Class Captcha
 * @package Plugin\jtl_google_recaptcha
 */
class Captcha
{
    public static function getCaptcha(string $type, PluginInterface $plugin): CaptchaInterface
    {
        return match ($type) {
            'invisible' => new CaptchaInvisible($plugin),
            default     => new CaptchaWidget($plugin),
        };
    }
}
