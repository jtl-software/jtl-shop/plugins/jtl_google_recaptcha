<?php

declare(strict_types=1);

namespace Plugin\jtl_google_recaptcha;

use JTL\Helpers\Request;
use JTL\Plugin\PluginInterface;

/**
 * Class AbstractCaptcha
 * @package Plugin\jtl_google_recaptcha
 */
abstract class AbstractCaptcha implements CaptchaInterface
{
    private const VERIFY_URL = 'https://www.google.com/recaptcha/api/siteverify';

    private PluginInterface $plugin;

    private string $siteKey;

    private string $secretKey;

    public function __construct(PluginInterface $plugin)
    {
        $this->plugin    = $plugin;
        $this->secretKey = $this->plugin->getConfig()->getValue('jtl_google_recaptcha_secretkey');
        $this->siteKey   = $this->plugin->getConfig()->getValue('jtl_google_recaptcha_sitekey');
    }

    /**
     * @inheritdoc
     */
    public function getSiteKey(): ?string
    {
        return $this->siteKey;
    }

    /**
     * @inheritdoc
     */
    public function getSecretKey(): ?string
    {
        return $this->secretKey;
    }

    protected function getPlugin(): PluginInterface
    {
        return $this->plugin;
    }

    /**
     * @inheritdoc
     */
    public function isConfigured(): bool
    {
        return !empty($this->secretKey) && !empty($this->siteKey);
    }

    /**
     * @inheritdoc
     */
    public function validate(array $requestData): bool
    {
        if (!$this->isConfigured()) {
            return true;
        }

        if (empty($requestData['g-recaptcha-response'])) {
            return false;
        }

        $jResponse = Request::make_http_request(self::VERIFY_URL, 5, [
            'secret'   => $this->getSecretKey(),
            'response' => $requestData['g-recaptcha-response'],
        ]);
        if (\is_string($jResponse)) {
            try {
                $result = \json_decode($jResponse, false, 512, \JSON_THROW_ON_ERROR);
            } catch (\JsonException) {
                return false;
            }

            return isset($result->success) && $result->success;
        }

        return false;
    }
}
