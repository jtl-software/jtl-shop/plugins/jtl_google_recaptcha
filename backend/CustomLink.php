<?php

declare(strict_types=1);

namespace Plugin\jtl_google_recaptcha\backend;

use Exception;
use JTL\Alert\Alert;
use JTL\Helpers\Text;
use JTL\Plugin\PluginInterface;
use JTL\Shop;
use JTL\Smarty\JTLSmarty;
use Plugin\jtl_google_recaptcha\Captcha;

/**
 * Class CustomLink
 * @package Plugin\jtl_google_recaptcha\backend
 */
class CustomLink
{
    private PluginInterface $plugin;

    private object $menu;

    private string $task;

    private array $requestData;

    private string $output = '';

    private JTLSmarty $smarty;

    /**
     * @param PluginInterface $plugin
     * @param object          $menu
     * @param array           $requestData
     * @param JTLSmarty       $smarty
     */
    private function __construct(PluginInterface $plugin, object $menu, array $requestData, JTLSmarty $smarty)
    {
        $this->plugin      = $plugin;
        $this->menu        = $menu;
        $this->requestData = Text::filterXSS($requestData);
        $this->task        = \strtolower($menu->name);
        $this->smarty      = $smarty;
    }

    /**
     * @param PluginInterface $plugin
     * @param object          $menu
     * @param array           $requestData
     * @param JTLSmarty       $smarty
     * @return CustomLink
     */
    public static function handleRequest(
        PluginInterface $plugin,
        object $menu,
        array $requestData,
        JTLSmarty $smarty
    ): self {
        $instance = new static($plugin, $menu, $requestData, $smarty);
        if ($instance->controller()) {
            return $instance->render();
        }

        return $instance;
    }

    private function taskTesting(): void
    {
        $captcha = Captcha::getCaptcha(
            $this->getPlugin()->getConfig()->getValue('jtl_google_recaptcha_type'),
            $this->getPlugin()
        );
        $data    = $this->getRequestData();
        try {
            if (isset($data['test_var'])) {
                $this->smarty->assign('reCaptchaValid', $captcha->validate(Text::filterXSS($_POST)))
                    ->assign('reCaptchaTest', Text::htmlentities($data['test_var']));
            }
            $pluginURL = \method_exists($this->getPlugin()->getPaths(), 'getBackendURL')
                ? $this->getPlugin()->getPaths()->getBackendURL()
                : Shop::getAdminURL() . '/plugin.php?kPlugin=' . $this->getPlugin()->getID();
            $this->smarty->assign('pluginURL', $pluginURL)
                ->assign('reCaptchaConfig', $captcha->isConfigured())
                ->assign('reCaptcha', $captcha->getMarkup());
        } catch (Exception $e) {
            Shop::Container()->getAlertService()->addAlert(Alert::TYPE_ERROR, $e->getMessage(), 'smartyError');
        }
    }

    protected function controller(): bool
    {
        if ($this->task === 'testing') {
            $this->taskTesting();
        }

        return true;
    }

    protected function getTemplate(): string
    {
        return 'template/' . $this->task . '.tpl';
    }

    public function getPlugin(): PluginInterface
    {
        return $this->plugin;
    }

    public function getMenu(): object
    {
        return $this->menu;
    }

    public function getRequestData(): array
    {
        return $this->requestData;
    }

    public function getOutput(): string
    {
        return $this->output;
    }

    public function render(): self
    {
        try {
            $this->output = $this->smarty->assign('requestData', $this->getRequestData())
                ->assign('kPlugin', $this->getPlugin()->getID())
                ->assign('kPluginAdminMenu', $this->getMenu()->kPluginAdminMenu)
                ->fetch($this->getPlugin()->getPaths()->getAdminPath() . $this->getTemplate());
        } catch (Exception $e) {
            $this->output = $e->getMessage();
        }

        return $this;
    }
}
